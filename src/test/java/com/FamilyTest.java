package com;
import com.Human;
import com.Family;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

class FamilyTest {
    public Family family;
    private Human father;
    private Human mother;
    private Human deleted_human;
    private Human added_human;

    com.Pet pet;

    @BeforeEach
    void familyInfo() {
        mother = new Human("Rafig", "Lahijov", 2002);
        father = new Human("Samir", "Lahijov", 1891);
        family = new Family(mother, father);
        Pet pet = new Pet(Species.CAT, "jane", 21, 8, new String[]{"run, take a nap, hide"});
        family.setPet(pet);
        deleted_human = new Human("deleted_human_1", "Deleted", 1882);
        family.addChild(deleted_human);
        added_human = new Human("added_human_1", "added", 1252);
        family.addChild(added_human);
    }

    @Test
    void testToString() {
        String expected = "com.Family{" +
                "mother=" + mother +
                ", father=" + father +
                ", children=" + Arrays.deepToString(family.children) +
                ", pet=" + pet +
                '}';
        Assertions.assertEquals(family.toString(), expected);
    }

    @Test
    void addChild() {
        family.addChild(added_human);
        assertTrue(((family.listOfChilderen()).contains((added_human))));

    }

    @Test
    void deleteChild() {
        assertTrue(family.deleteChild(0));
        assertFalse(family.listOfChilderen().contains(deleted_human));
    }

    @Test
    void deleteNonExistingChild() {
        assertFalse(family.deleteChild(10));
    }

    @Test
    void countFamily() {
         Assertions.assertEquals(4,family.countFamily());
    }
}
